package main

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Log struct {
	gorm.Model
	UserID    uint       `json:"UserID"` // foreign key
	Title     string     `json:"Title"`
	Date      time.Time  `json:"Date"`
	Exercises []Exercise `json:"Exercises"`
}
