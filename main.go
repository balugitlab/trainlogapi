package main

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // is used in production
	_ "github.com/jinzhu/gorm/dialects/sqlite"   // for dev
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var db *gorm.DB
var jwtSecret string
var port string
var dbType string
var database string

func main() {

	// ENV variables
	production := true

	jwtSecret = os.Getenv("JWTSECRET")
	port = os.Getenv("PORT")

	if jwtSecret == "" {
		jwtSecret = "DeveloperSecret"
	}

	if port == "" {
		port = "1323"

	}

	if production {
		dbType = "postgres"
		database = os.Getenv("DATABASE_URL")
		if database == "" {
			panic("Failed to get DATABASE_URL from env!")

		}
	} else {
		dbType = "sqlite3"
		database = "test.db"
	}

	// DB stuff
	var err error
	db, err = gorm.Open(dbType, database)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	// DB migrations
	migrate()

	// // for debugging
	// db.LogMode(true)

	// Server
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	// Routes

	// Restricted Routes Group JWT Requirerd
	r := e.Group("/")
	r.Use(middleware.JWT([]byte(jwtSecret)))

	//User Routes
	r.GET("user", getUser)
	e.PUT("/user/:id", putUser)
	r.PATCH("user/:id", patchUser)
	r.DELETE("/user/:id", deleteUser)

	e.POST("/login/:email/:password", login)
	e.POST("/user", postUser) // aka createUser

	//Log Routes
	r.GET("log/:id", getLog)
	r.GET("logs/:amount", getLogs)
	r.POST("log", postLog)
	r.PUT("log", putLog)
	r.PATCH("log", patchLog)
	r.DELETE("log/:id", deleteLog)

	// Exercise Routes
	r.DELETE("exercise/:id", deleteExercise)

	// Server
	e.Logger.Fatal(e.Start(":" + port))
}
