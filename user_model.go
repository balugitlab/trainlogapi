package main

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Logs     []Log
	Email    string `json:"Email"`
	Password string `json:"Password"`
	Username string `json:"Username"`
}
