# TrainLogAPI

API for the TrainLogWebsite

currently hosted at https://trainlogapi.herokuapp.com

### Frontend
- Hosted at https://trainlog.surge.sh/
- Repo at https://gitlab.com/balugitlab/trainlogwebsite

### Response/s

All responses return JSON

On error the  appropriate  response status plus an optional JSON object that contains a message key and the error will be returned e.g

#### Status:
`
StatusNotAcceptable == 406
`
#### Body:
```json
{
  "Message": "content was not acceptable because...",
}
```

### PostLog request/response reference
```json
{
  "ID": 4,
  "CreatedAt": "2019-07-10T02:08:45.087134447+02:00",
  "UpdatedAt": "2019-07-10T02:08:45.087134447+02:00",
  "DeletedAt": null,
  "UserID": 13,
  "Title": "exer4",
  "Date": "2019-07-08T19:57:38.49040079+02:00",
  "Exercises": [
    {
      "ID": 5,
      "CreatedAt": "2019-07-10T02:08:45.087369394+02:00",
      "UpdatedAt": "2019-07-10T02:08:45.087369394+02:00",
      "DeletedAt": null,
      "LogID": 4,
      "Name": "Benchwithweight0",
      "Weight": 0,
      "Reps": 10,
      "Sets": 11,
      "Rest": ""
    },
    {
      "ID": 6,
      "CreatedAt": "2019-07-10T02:08:45.087511074+02:00",
      "UpdatedAt": "2019-07-10T02:08:45.087511074+02:00",
      "DeletedAt": null,
      "LogID": 4,
      "Name": "",
      "Weight": 2,
      "Reps": 3,
      "Sets": 4,
      "Rest": ""
    }
  ]
}
```
