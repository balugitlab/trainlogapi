package main

func migrate() {
	// Migrate the schema/s
	db.AutoMigrate(&Log{})
	db.AutoMigrate(&Exercise{})
	db.AutoMigrate(&User{})
}
