package main

import (
	"net/http"
	"strconv"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func getLog(c echo.Context) error {

	// get user id
	u := c.Get("user").(*jwt.Token)
	claims := u.Claims.(jwt.MapClaims)
	userId := uint(claims["userid"].(float64))

	// get requested log id
	rawlogId, _ := strconv.Atoi(c.Param("id"))
	logId := uint(rawlogId)

	dbLog := new(Log)
	exercises := new([]Exercise)

	db.Where(&Log{UserID: userId}).First(dbLog, logId)
	db.Model(dbLog).Related(exercises)

	dbLog.Exercises = *exercises

	return c.JSON(http.StatusOK, dbLog)
}

func getLogs(c echo.Context) error {
	// gets $amount of logs ordered by userdate(not CreatedAt) DESC e.g recent logs first

	userId := getUserIdJWT(c)

	// get requested amount
	rawAmount, _ := strconv.Atoi(c.Param("amount"))
	amount := uint(rawAmount)

	logs := new([]Log)

	db.Where(&Log{UserID: userId}).Order("date desc").Limit(amount).Find(logs)

	return c.JSON(http.StatusOK, logs)
}

func postLog(c echo.Context) error {

	requestLog := new(Log)
	if err := c.Bind(requestLog); err != nil {
		return err
	}

	// setting the userid of the posted log to the user id of the user
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userId := uint(claims["userid"].(float64))
	requestLog.UserID = userId

	db.Create(requestLog)

	return c.JSON(http.StatusCreated, requestLog)
}

func putLog(c echo.Context) error {

	return c.String(http.StatusOK, "")
}

func patchLog(c echo.Context) error {

	// this handler is not optimal view below for more info
	// but it should be sufficient for now

	userId := getUserIdJWT(c)

	requestLog := new(Log)
	if err := c.Bind(requestLog); err != nil {
		return err
	}

	// getting the db log in order to compare it to the request log
	dbLog := new(Log)
	db.Where(&Log{UserID: userId}).First(dbLog, requestLog.Model.ID)

	// auth checks
	// check if ID matches with the db log
	if dbLog.Model.ID != requestLog.Model.ID {
		return c.NoContent(http.StatusUnauthorized)
	}

	// check if Exercises for that log are valid
	// currently we just check for LogID so users could mess with their own exercises
	for _, e := range requestLog.Exercises {
		if e.LogID != dbLog.Model.ID {
			return c.NoContent(http.StatusUnauthorized)
		}
	}

	// All checks passed

	// Gorm will not update Zero falues like 0, "" or false i can not really think
	// of a situation where one would want to update it to such falues so we do not handle that
	db.Model(requestLog).Updates(requestLog)

	return c.JSON(http.StatusOK, requestLog)
}

func deleteLog(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
