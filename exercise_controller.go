package main

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

func deleteExercise(c echo.Context) error {

	userId := getUserIdJWT(c)
	// get id
	rawId, _ := strconv.Atoi(c.Param("id"))
	id := uint(rawId)

	// check if allowed
	dbExer := new(Exercise)
	db.First(dbExer, id)

	dbLog := new(Log)
	db.First(dbLog, dbExer.LogID)

	if dbLog.UserID == userId {
		// exer belongs to user and can be deleted
		db.Delete(dbExer)
		return c.NoContent(http.StatusNoContent)
	}

	// user tried to either delete another users exer or it does not exist
	return c.NoContent(http.StatusUnauthorized)
}
