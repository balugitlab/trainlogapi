package main

import "github.com/jinzhu/gorm"

type Exercise struct {
	gorm.Model
	LogID  uint // foreign key
	Name   string
	Weight int
	Reps   int
	Sets   int
	Rest   string
}
