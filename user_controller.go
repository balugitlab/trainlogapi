package main

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func getUser(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userId := uint(claims["userid"].(float64))

	dbuser := new(User)
	db.First(dbuser, userId)

	return c.JSON(http.StatusOK, dbuser)
}

func postUser(c echo.Context) error {
	// aka createUser
	u := new(User)
	if err := c.Bind(u); err != nil {
		return err
	}

	// Check if email is unique
	existingUser := new(User)
	db.Where(&User{Email: u.Email}).First(existingUser)

	if (User{}.Email) != existingUser.Email {
		// email already exists in DB
		return c.JSON(http.StatusNotAcceptable, ErrorResponse{Message: "Email already exists"})
	}

	// Hashing PW
	u.Password, _ = HashPw(u.Password)

	// All checks passed
	db.Create(u)
	return c.JSON(http.StatusOK, u)
}

func putUser(c echo.Context) error {
	// TODO:
	return c.String(http.StatusOK, "Hello, World!")
}
func patchUser(c echo.Context) error {
	// TODO:
	return c.String(http.StatusOK, "Hello, World!")
}

func deleteUser(c echo.Context) error {
	// TODO:
	return c.String(http.StatusOK, "Hello, World!")
}

func login(c echo.Context) error {
	email := c.Param("email")
	password := c.Param("password")

	existingUser := new(User)
	db.Where(&User{Email: email}).First(existingUser)

	if (User{}.Email) == existingUser.Email {
		// User with that email does not exists
		return echo.ErrUnauthorized
	}

	// Check PW
	if !CheckPw(password, existingUser.Password) {
		return echo.ErrUnauthorized
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)

	// God knows why but for some reason claims["userid"] becomes a float64 when retriving it
	// afaik this does not break anything atm and one can just convert it to a int or whatever
	claims["userid"] = existingUser.Model.ID

	// below are non tested options that will turn the userid into a string so the userid gets set as a string
	// claims["userid"] = fmt.Sprint(existingUser.Model.ID)
	// claims["userid"] = strconv.FormatUint(uint64(existingUser.Model.ID), 10)

	claims["email"] = existingUser.Email
	claims["username"] = existingUser.Username
	claims["exp"] = time.Now().Add(time.Hour * 8766 * 10).Unix() // 10 years

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}
