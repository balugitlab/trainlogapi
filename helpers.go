package main

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func getUserIdJWT(c echo.Context) (userId uint) {

	u := c.Get("user").(*jwt.Token)
	claims := u.Claims.(jwt.MapClaims)
	userId = uint(claims["userid"].(float64))
	return
}
